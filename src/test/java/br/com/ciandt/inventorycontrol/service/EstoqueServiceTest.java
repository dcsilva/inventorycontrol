package br.com.ciandt.inventorycontrol.service;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Bebida;
import br.com.ciandt.inventorycontrol.model.Estoque;
import br.com.ciandt.inventorycontrol.model.Historico;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.properties.InitialProperties;
import br.com.ciandt.inventorycontrol.properties.MessagesProperties;
import br.com.ciandt.inventorycontrol.repository.HistoricoRepository;
import br.com.ciandt.inventorycontrol.util.SecaoUtil;
import br.com.ciandt.inventorycontrol.wrapper.BebidaWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.misusing.InvalidUseOfMatchersException;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(InitialProperties.class)
public class EstoqueServiceTest {

    public static String TIPO_BEBIDA_ALCOOLICA_DESCRICAO = "ALCOOLICA";
    public static String TIPO_BEBIDA_NAO_ALCOOLICA_DESCRICAO = "NAO_ALCOOLICA";

    @Mock
    public BebidaService bebidaService;

    @Mock
    public MessagesProperties messagesProperties;

    @Mock
    public SecaoService secaoService;

    @Mock
    public HistoricoRepository historicoRepository;

    @InjectMocks
    public HistoricoService historicoService;

    @InjectMocks
    public SecaoUtil secaoUtil;

    @InjectMocks
    public EstoqueService estoqueService;

    public Map<String, Object> retornos = new HashMap<>();

    @Before
    public void init() {
        mockBebidasRetorno();
        mockHistoricoRepository();
        estoqueService.setHistoricoService(historicoService);
    }

    @Test
    public void quandoRegistrarBebidaAlcoolica_EntaoRetornaStatus200() {
        BebidaWrapper bebidaWrapper =
                new BebidaWrapper(400.00, TIPO_BEBIDA_ALCOOLICA_DESCRICAO, "Diego");

        Bebida bebida = (Bebida) retornos.get("bebida_alcoolica_secao1");
        mockBebidaService(new Bebida(bebidaWrapper), 1);
        mockSecaoService(getMockEstoqueComSecoes().getSecoes().get(0));

        Bebida resultado = estoqueService.registraEntradaBebida(bebidaWrapper);

        bebida.setSecao(secaoComSaldoDebitado(bebida));

        bebida.getSecao().setBebidas(null);
        bebida.getSecao().setEstoque(null);
        resultado.getSecao().setBebidas(null);
        resultado.getSecao().setEstoque(null);

        assertEquals(bebida, resultado);
    }

    @Test
    public void quandoRegistrarBebidaNaoAlcoolica_EntaoRetornaStatus200() {
        BebidaWrapper bebidaWrapper =
                new BebidaWrapper(400.00, TIPO_BEBIDA_NAO_ALCOOLICA_DESCRICAO, "Diego");

        Bebida bebida = (Bebida) retornos.get("bebida_nao_alcoolica_secao1");
        mockBebidaService(new Bebida(bebidaWrapper), 1);
        mockSecaoService(getMockEstoqueComSecoes().getSecoes().get(0));

        Bebida resultado = estoqueService.registraEntradaBebida(bebidaWrapper);

        bebida.setSecao(secaoComSaldoDebitado(bebida));

        bebida.getSecao().setBebidas(null);
        bebida.getSecao().setEstoque(null);
        resultado.getSecao().setBebidas(null);
        resultado.getSecao().setEstoque(null);

        assertEquals(bebida, resultado);
    }

    private Secao secaoComSaldoDebitado(Bebida bebida) {
        Secao secao = getMockEstoqueComSecoes().getSecoes().get(0);
        return estoqueService.alteraLimiteVolumeSecao(bebida, secao);
    }

    public Estoque getMockEstoque() {
        return Estoque.builder().
                id(1).
                build();
    }

    public Secao getMockSecao(Integer id, TipoBebida tipoBebida, Estoque estoque) {
        return Secao.builder().
                id(id).
                capacidaLitros(500.00).
                tipoBebidaArmazenada(tipoBebida).
                estoque(estoque).
                bebidas(new ArrayList<>()).
                build();
    }

    public Estoque getMockEstoqueComSecoes() {
        Estoque estoque = getMockEstoque();
        List<Secao> secoes = new ArrayList<>();

        IntStream.range(0, 5).forEach(
                value -> secoes.add(getMockSecao(value + 1, TipoBebida.ALCOOLICA, estoque))
        );

        estoque.setSecoes(secoes);

        return estoque;
    }

    private void mockHistoricoRepository() {
        when(historicoRepository.save(any(Historico.class))).then(i -> i.getArgumentAt(0, Historico.class));
    }

    public void mockBebidaService(Bebida bebida) {
        when(bebidaService.cadastrar(bebida)).thenReturn(bebida);
        when(bebidaService.save(bebida)).thenReturn(bebida);
    }

    public void mockBebidaService(Bebida bebida, Integer id) {
        Bebida bebidaRetorno = new Bebida(bebida);
        bebidaRetorno.setId(id);
        when(bebidaService.cadastrar(any(Bebida.class))).then(i -> i.getArgumentAt(0, Bebida.class));
        when(bebidaService.save(bebida)).thenReturn(bebida.getId() == null ? bebidaRetorno : bebida);
    }

    public void mockSecaoService(Secao secao) {
        when(secaoService.save(any(Secao.class))).thenAnswer(
                invocation -> {
                    Secao argument = (Secao) invocation.getArguments()[0];
                    if (argument.equals(secao)) {
                        return secao;
                    } else {
                        return argument;
                    }
                }
        );
        when(secaoService.buscaSecaoBebida(any(Bebida.class))).thenReturn(secao);
    }

    public void mockBebidasRetorno() {
        Estoque estoque = getMockEstoqueComSecoes();

        Bebida bebidaAlcoolica = Bebida.builder().
                id(1).
                tipoBebida(TipoBebida.ALCOOLICA).
                medidaLitro(400.00).
                secao(estoque.getSecoes().get(0)).
                build();

        Bebida bebidaNaoAlcoolica = Bebida.builder().
                id(1).
                tipoBebida(TipoBebida.NAO_ALCOOLICA).
                medidaLitro(400.00).
                secao(estoque.getSecoes().get(0)).
                build();

        retornos.put("bebida_alcoolica_secao1", bebidaAlcoolica);
        retornos.put("bebida_nao_alcoolica_secao1", bebidaNaoAlcoolica);
    }

}
