package br.com.ciandt.inventorycontrol.controller;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.service.EstoqueService;
import br.com.ciandt.inventorycontrol.service.HistoricoService;
import br.com.ciandt.inventorycontrol.wrapper.BebidaArmazenamentoWrapper;
import br.com.ciandt.inventorycontrol.wrapper.BebidaWrapper;
import br.com.ciandt.inventorycontrol.wrapper.ConsultaHistoricoBySecaoWrapper;
import br.com.ciandt.inventorycontrol.wrapper.ConsultaHistoricoByTipoBebidaWrapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inventory")
public class EstoqueController {

    @Autowired
    private EstoqueService estoqueService;

    @Autowired
    private HistoricoService historicoService;

    @ApiOperation(
            value = "Cadastrar uma bebida no estoque", response = ResponseEntity.class, notes = "Essa operação realiza o cadastro de uma bebida por tipo.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/entradaBebida", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity entradaBebida(BebidaWrapper bebidaWrapper) {
        return ResponseEntity.ok(estoqueService.registraEntradaBebida(bebidaWrapper));
    }

    @ApiOperation(
            value = "Consulta das bebidas armazenadas em cada seção",
            response = ResponseEntity.class,
            notes = "Essa operação realiza uma consulta retornando as seções e a descrição da quantidade de volume armazenado.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/listarSecoes", method = RequestMethod.GET)
    private @ResponseBody
    ResponseEntity listarSecoes() {
        return ResponseEntity.ok(estoqueService.listarSecoes());
    }

    @ApiOperation(
            value = "Consulta o total de bebida armazenada por tipo.",
            response = ResponseEntity.class,
            notes = "Essa operação realiza uma consulta retornando o total de volume armazenado por tipo de bebida.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/listarBebidas", method = RequestMethod.GET)
    private @ResponseBody
    ResponseEntity listarBebidas() {
        return ResponseEntity.ok(estoqueService.listarBebidas());
    }

    @ApiOperation(
            value = "Consulta uma determinada seção para armazenamento da bebida por tipo e volume",
            response = ResponseEntity.class,
            notes = "Essa operação realiza a consulta de uma seção para armazenamento de um volume de bebida")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/consultaArmazenamentoBebida", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity consultaArmazenamentoBebida(BebidaArmazenamentoWrapper bebidaArmazenamentoWrapper) {
        return ResponseEntity.ok(estoqueService.consultaArmazenamentoBebida(bebidaArmazenamentoWrapper));
    }

    @ApiOperation(
            value = "Consulta quais as seções disponíveis para venda de determinado tipo de bebida.",
            response = ResponseEntity.class,
            notes = "Essa operação realiza a consulta de uma seção disponível para venda de determinado tipo de bebida.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/consultaSecaoVendaBebida", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity consultaSecaoVendaBebida(TipoBebida tipoBebida) {
        return ResponseEntity.ok(estoqueService.consultaSecaoVendaBebida(tipoBebida));
    }


    @ApiOperation(
            value = "Registra uma venda de determinado tipo de bebida.",
            response = ResponseEntity.class,
            notes = "Essa operação realiza o registro da venda de um determinado tipo de bebida.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @RequestMapping(value = "/vendaBebida", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity vendaBebida(BebidaWrapper bebidaWrapper) {
        return ResponseEntity.ok(estoqueService.vendaBebida(bebidaWrapper));
    }

    @ApiOperation(
            value = "Consulta do histórico de entradas e saídas por tipo de bebida.",
            response = ResponseEntity.class,
            notes = "Essa operação realiza a consulta do histórico de entrada e saida por tipo de bebida")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tipoBebida", value = "Tipo da Bebida", paramType = "query"),
            @ApiImplicitParam(name = "tipoOrdenacao", value = "Campo que será ordenado o resultado" , paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "Ordem do resultado", paramType = "query")
    })
    @RequestMapping(value = "/consultaHistoricoPorBebida", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity consultaHistoricoPorBebida(ConsultaHistoricoByTipoBebidaWrapper consultaHistoricoByTipoBebidaWrapper) {
        return ResponseEntity.ok(historicoService.consultaHistorico(consultaHistoricoByTipoBebidaWrapper));
    }

    @ApiOperation(
            value = "Consulta do histórico de entradas e saídas por seção.",
            response = ResponseEntity.class,
            notes = "Essa operação realiza a consulta do histórico de entrada e saida por seção.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um ResponseEntity com uma mensagem de sucesso", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "secao", value = "Numero da Seção 1, 2, 3, 4 ou 5", paramType = "query", allowableValues = "1, 2, 3, 4, 5"),
            @ApiImplicitParam(name = "tipoOrdenacao", value = "Campo que será ordenado o resultado" , paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "Ordem do resultado", paramType = "query")
    })
    @RequestMapping(value = "/consultaHistoricoPorSecao", method = RequestMethod.POST)
    private @ResponseBody
    ResponseEntity consultaHistoricoPorSecao(ConsultaHistoricoBySecaoWrapper consultaHistoricoBySecaoWrapper) {
        return ResponseEntity.ok(historicoService.consultaHistorico(consultaHistoricoBySecaoWrapper));
    }
}
