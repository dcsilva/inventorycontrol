package br.com.ciandt.inventorycontrol.service;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Bebida;
import br.com.ciandt.inventorycontrol.model.Estoque;
import br.com.ciandt.inventorycontrol.model.Historico;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.repository.EstoqueRepository;
import br.com.ciandt.inventorycontrol.repository.HistoricoRepository;
import br.com.ciandt.inventorycontrol.repository.SecaoRepository;
import br.com.ciandt.inventorycontrol.wrapper.SecaoViewWrapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class SecaoService {

    @Autowired
    private SecaoRepository secaoRepository;

    @Autowired
    private HistoricoRepository historicoRepository;

    @Autowired
    private EstoqueRepository estoqueRepository;

    public Secao save(Secao secao) {
        return secaoRepository.save(secao);
    }

    public List<Secao> findAll(){
        Estoque estoque = estoqueRepository.findOne(1);
        return estoque.getSecoes();
    }

    public List<SecaoViewWrapper> listarSecoes() {
        List<Secao> secoes = findAll();
        List<SecaoViewWrapper> secoesView = new ArrayList<>();

        secoes.forEach(secao -> {secoesView.add(new SecaoViewWrapper(secao));});

        return secoesView;
    }

    public Secao buscaSecaoBebida(Bebida bebida) {
        List<Secao> secoes = findAll();
        Secao secao = secoes.stream().filter(s -> bebida.getTipoBebida().equals(s.getTipoBebidaArmazenada())
                && s.getCapacidaLitros() >= bebida.getMedidaLitro()
                && verificaSePodeArmazenarHoje(s, bebida))
                .findFirst().orElse(null);

        if (secao == null){
            secao = secoes.stream().filter(s -> s.getTipoBebidaArmazenada() == null
                    && bebida.getMedidaLitro() < bebida.getTipoBebida().getCapacidadeSecao()
                    && verificaSePodeArmazenarHoje(s, bebida))
                    .findFirst().orElse(null);
        }

        return secao;
    }

    public boolean verificaSePodeArmazenarHoje(Secao secao, Bebida bebida){
        if (TipoBebida.NAO_ALCOOLICA.equals(bebida.getTipoBebida())){
            List<Historico>  historicos =
                    historicoRepository.findAllBySecaoAndTipoBebida(
                    secao, TipoBebida.ALCOOLICA);

            historicos = historicos.stream().filter(h -> h.getDataHora().getDate() != new Date().getDate())
                    .collect(Collectors.toList());

            if (historicos != null && !historicos.isEmpty()){
                return false;
            }
        }
        return true;
    }

    public List<Secao> consultaSecaoVendaBebida(TipoBebida tipoBebida) {
        return secaoRepository.findAllByTipoBebidaArmazenada(tipoBebida);
    }

    public Secao findById(Integer secao) {
        return secaoRepository.findOne(secao);
    }
}
