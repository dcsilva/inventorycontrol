package br.com.ciandt.inventorycontrol.service;

import br.com.ciandt.inventorycontrol.util.SecaoUtil;
import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Bebida;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.repository.BebidaRepository;
import br.com.ciandt.inventorycontrol.wrapper.BebidaTotalWrapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Data
public class BebidaService {

    @Autowired
    private SecaoService secaoService;

    @Autowired
    private SecaoUtil secaoUtil;

    @Autowired
    private BebidaRepository bebidaRepository;

    public Bebida cadastrar(Bebida bebida){
        return bebidaRepository.save(bebida);
    }

    public Bebida save(Bebida bebida) {
        return bebidaRepository.save(bebida);
    }

    public List<BebidaTotalWrapper> listarTotalBebidasPorTipo() {
        List<BebidaTotalWrapper> bebidaTotal = new ArrayList<>();
        List<Secao> secoes = secaoService.findAll();
        Arrays.asList(TipoBebida.values()).forEach(tipoBebida -> {
            Double totalSoma = secoes.stream().filter(s ->
                    s.getTipoBebidaArmazenada() != null && s.getTipoBebidaArmazenada().equals(tipoBebida))
                    .mapToDouble(s -> secaoUtil.calculaQuantidadeArmazenada(s.getCapacidaLitros(), tipoBebida))
                    .sum();
            bebidaTotal.add(new BebidaTotalWrapper(tipoBebida, totalSoma));
        });
        return bebidaTotal;
    }
}
