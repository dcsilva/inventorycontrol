package br.com.ciandt.inventorycontrol.service;

import br.com.ciandt.inventorycontrol.domain.TipoHistorico;
import br.com.ciandt.inventorycontrol.model.Bebida;
import br.com.ciandt.inventorycontrol.model.Historico;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.repository.HistoricoRepository;
import br.com.ciandt.inventorycontrol.wrapper.BebidaWrapper;
import br.com.ciandt.inventorycontrol.wrapper.ConsultaHistoricoBySecaoWrapper;
import br.com.ciandt.inventorycontrol.wrapper.ConsultaHistoricoByTipoBebidaWrapper;
import br.com.ciandt.inventorycontrol.wrapper.ConsultaHistoricoWrapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class HistoricoService {

    @Autowired
    private HistoricoRepository historicoRepository;

    @Autowired
    private SecaoService secaoService;

    public void registraEntrada(Bebida bebida, String responsavel) {
        Historico historico = new Historico();
        historico.setResponsavel(responsavel);
        historico.setSecao(bebida.getSecao());
        historico.setTipoHistorico(TipoHistorico.ENTRADA);
        historico.setVolume(bebida.getMedidaLitro());
        historico.setTipoBebida(bebida.getTipoBebida());
        historicoRepository.save(historico);
    }

    public void registraSaida(Secao secao, BebidaWrapper bebidaWrapper) {
        Historico historico = new Historico();
        historico.setResponsavel(bebidaWrapper.getNomeResponsavel());
        historico.setSecao(secao);
        historico.setTipoHistorico(TipoHistorico.SAIDA);
        historico.setVolume(bebidaWrapper.getMedida());
        historico.setTipoBebida(secao.getTipoBebidaArmazenada());
        historicoRepository.save(historico);
    }

    public List<Historico> consultaHistorico(ConsultaHistoricoWrapper consultaHistoricoWrapper) {
        if (consultaHistoricoWrapper instanceof ConsultaHistoricoByTipoBebidaWrapper){

            ConsultaHistoricoByTipoBebidaWrapper consultaHistoricoByTipoBebidaWrapper =
                    (ConsultaHistoricoByTipoBebidaWrapper) consultaHistoricoWrapper;

            Example<Historico> example =
                    Example.of(new Historico(consultaHistoricoByTipoBebidaWrapper.getTipoBebida()));

            return historicoRepository.findAll(
                    example, new Sort(
                            Sort.Direction.fromString(consultaHistoricoByTipoBebidaWrapper.getSort().getValue()),
                            consultaHistoricoByTipoBebidaWrapper.getTipoOrdenacao().getValue()
                    )
            );
        }else {
            ConsultaHistoricoBySecaoWrapper consultaHistoricoBySecaoWrapper =
                    (ConsultaHistoricoBySecaoWrapper) consultaHistoricoWrapper;

            Secao secao = secaoService.findById(consultaHistoricoBySecaoWrapper.getSecao());

            Example<Historico> example =
                    Example.of(new Historico(secao));

            return historicoRepository.findAll(
                    example, new Sort(
                            Sort.Direction.fromString(consultaHistoricoBySecaoWrapper.getSort().getValue()),
                            consultaHistoricoBySecaoWrapper.getTipoOrdenacao().getValue()
                    )
            );
        }
    }
}
