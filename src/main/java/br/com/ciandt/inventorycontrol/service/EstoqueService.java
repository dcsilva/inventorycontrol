package br.com.ciandt.inventorycontrol.service;

import br.com.ciandt.inventorycontrol.properties.MessagesProperties;
import br.com.ciandt.inventorycontrol.util.SecaoUtil;
import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.exception.FullStockException;
import br.com.ciandt.inventorycontrol.exception.StockNotAvailableException;
import br.com.ciandt.inventorycontrol.model.Bebida;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.wrapper.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Data
public class EstoqueService {

    @Autowired
    private BebidaService bebidaService;

    @Autowired
    private SecaoService secaoService;

    @Autowired
    private HistoricoService historicoService;

    @Autowired
    private SecaoUtil secaoUtil;

    @Autowired
    private MessagesProperties messagesProperties;

    public Bebida registraEntradaBebida(BebidaWrapper bebidaWrapper) {
        Bebida bebida = new Bebida(bebidaWrapper);
        bebida = bebidaService.save(bebida);
        Secao secao = preparaSecao(bebida);
        bebida.setSecao(secao);
        atualizaSecao(secao, bebida);
        historicoService.registraEntrada(bebida, bebidaWrapper.getNomeResponsavel());
        return bebidaService.cadastrar(bebida);
    }

    public Secao preparaSecao(Bebida bebida){
        Secao secao = secaoService.buscaSecaoBebida(bebida);

        if (secao == null){
            throw new FullStockException(messagesProperties.getMessage("error.full_stock_when_storing"));
        }else if (secao.getTipoBebidaArmazenada() == null){
            secao.setTipoBebidaArmazenada(bebida.getTipoBebida());
            secao.setCapacidaLitros(bebida.getTipoBebida().getCapacidadeSecao());
        }
        return secao;
    }

    private Secao atualizaSecao(Secao secao, Bebida bebida) {
        secao = alteraLimiteVolumeSecao(bebida, secao);
        List<Bebida> bebidas = secao.getBebidas();
        bebidas.add(bebida);
        secao.setBebidas(bebidas);
        return secaoService.save(secao);
    }

    public Secao alteraLimiteVolumeSecao(Bebida bebida, Secao secao) {
        Double novaCapacidade = secao.getCapacidaLitros() - bebida.getMedidaLitro();
        secao.setCapacidaLitros(novaCapacidade);
        return secaoService.save(secao);
    }

    public List<SecaoViewWrapper> listarSecoes() {
        return secaoService.listarSecoes();
    }

    public List<BebidaTotalWrapper> listarBebidas() {
        return bebidaService.listarTotalBebidasPorTipo();
    }

    public Map<String, Object> consultaArmazenamentoBebida(BebidaArmazenamentoWrapper bebidaArmazenamentoWrapper) {
        Secao secao = secaoService.buscaSecaoBebida(new Bebida(bebidaArmazenamentoWrapper));
        Map<String, Object> secaoMap = new HashMap<>();
        if (secao == null){
            throw new FullStockException(messagesProperties.getMessage("error.full_stock_query"));
        }else {
            secaoMap.put("Secao", secao.getId());
            if (secao.getTipoBebidaArmazenada() != null){
                secaoMap.put("Tipo", secao.getTipoBebidaArmazenada());
            }else{
                secaoMap.put("Tipo", "Tipo ainda não definido, Seção vazia hoje!");
            }
            if (secao.getCapacidaLitros() != null){
                secaoMap.put("Volume disponivel para armazenamento", secao.getCapacidaLitros());
            }else{
                secaoMap.put("Volume disponivel para armazenamento", "400L caso seja bebida não " +
                        "alcoólica e 500L caso seja alcoólica");
            }
        }
        return secaoMap;
    }

    public Map<String, Object> consultaSecaoVendaBebida(TipoBebida tipoBebida) {
        List<Secao> secoes = secaoService.consultaSecaoVendaBebida(tipoBebida);

        Map<String, Object> secaoMap = new HashMap<>();

        secoes.forEach(secao -> {
            SecaoViewWrapper secaoViewWrapper = new SecaoViewWrapper(secao);
            secaoMap.put("Secao "+secao.getId(), secaoViewWrapper);
        });
        return secaoMap;
    }

    public BebidaWrapper vendaBebida(BebidaWrapper bebidaWrapper) {
        List<Secao> secoes = secaoService.consultaSecaoVendaBebida(bebidaWrapper.getTipoBebida());

        Secao secao = secoes.stream().filter(s ->
                secaoUtil.calculaQuantidadeArmazenada
                        (s.getCapacidaLitros(), bebidaWrapper.getTipoBebida()) >= bebidaWrapper.getMedida() &&
                        s.getTipoBebidaArmazenada() != null &&
                        s.getTipoBebidaArmazenada().equals(bebidaWrapper.getTipoBebida()))
                .findFirst().orElse(null);

        if (secao == null){
            List<Secao> secoesTipo = secoes.stream().filter(s ->
                        s.getTipoBebidaArmazenada() != null &&
                        s.getTipoBebidaArmazenada().equals(bebidaWrapper.getTipoBebida()))
                    .collect(Collectors.toList());

            Double total = secoesTipo.stream().filter(Objects::nonNull)
                    .mapToDouble(s -> secaoUtil.calculaQuantidadeArmazenada(s.getCapacidaLitros(), s.getTipoBebidaArmazenada()))
                    .sum();

            if (total < bebidaWrapper.getMedida()){
                throw new StockNotAvailableException(messagesProperties.getMessage("error.stock_not_available"));
            }
            darBaixaDoEstoque(bebidaWrapper, secoesTipo);
        }else {
            darBaixaDoEstoque(bebidaWrapper, secao);
        }
        return bebidaWrapper;
    }

    private void darBaixaDoEstoque(BebidaWrapper bebidaWrapper, List<Secao> secoesTipo) {
        List<Secao> secoes = new ArrayList<>();
        Double total = 0.0;
        for (Secao secao: secoesTipo) {
            total = total + secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada());
            secoes.add(secao);
            if (total >= bebidaWrapper.getMedida()){
                break;
            }
        }
        total = 0.0;
        for (Secao secao: secoes) {
            if ((total+secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada()))
                    < bebidaWrapper.getMedida()){
                total = total+secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada());
                historicoService.registraSaida(secao, new BebidaWrapper(
                        secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada()),
                        bebidaWrapper.getTipoBebida().toString(),
                        bebidaWrapper.getNomeResponsavel()));
                esvaziaSecao(secao);
            }else {
                Double falta = bebidaWrapper.getMedida() - total;
                Double quantidadeSessao = secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada());
                quantidadeSessao = quantidadeSessao - falta;
                secao.setCapacidaLitros(secao.getTipoBebidaArmazenada().getCapacidadeSecao() - quantidadeSessao);
                if (secao.getCapacidaLitros().equals(0.0)){
                    esvaziaSecao(secao);
                }else{
                    secaoService.save(secao);
                }
                historicoService.registraSaida(secao, new BebidaWrapper(
                        falta,
                        bebidaWrapper.getTipoBebida().toString(),
                        bebidaWrapper.getNomeResponsavel()));
            }
        }
    }

    private void darBaixaDoEstoque(BebidaWrapper bebidaWrapper, Secao secao) {
        Double quantidadeSessao = secaoUtil.calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada());
        quantidadeSessao = quantidadeSessao - bebidaWrapper.getMedida();
        secao.setCapacidaLitros(secao.getTipoBebidaArmazenada().getCapacidadeSecao() - quantidadeSessao);
        if (quantidadeSessao.equals(0.0)){
            esvaziaSecao(secao);
        }else{
            secaoService.save(secao);
        }
        historicoService.registraSaida(secao, bebidaWrapper);
    }

    private void esvaziaSecao(Secao secao) {
        secao.setCapacidaLitros(null);
        secao.setTipoBebidaArmazenada(null);
        secao.setBebidas(null);
        secaoService.save(secao);
    }
}
