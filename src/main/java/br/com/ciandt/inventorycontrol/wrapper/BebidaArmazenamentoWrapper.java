package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BebidaArmazenamentoWrapper {

    private Double volume;
    private TipoBebida tipoBebida;

    public BebidaArmazenamentoWrapper(Double volume, String tipoBebida) {
        this.volume = volume;
        this.tipoBebida = TipoBebida.valueOf(tipoBebida);
    }
}
