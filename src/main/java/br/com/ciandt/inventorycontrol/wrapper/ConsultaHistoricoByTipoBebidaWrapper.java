package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaHistoricoByTipoBebidaWrapper extends ConsultaHistoricoWrapper{
    private TipoBebida tipoBebida;
}
