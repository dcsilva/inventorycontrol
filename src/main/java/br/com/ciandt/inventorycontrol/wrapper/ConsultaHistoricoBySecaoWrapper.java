package br.com.ciandt.inventorycontrol.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaHistoricoBySecaoWrapper extends ConsultaHistoricoWrapper{
    private Integer secao;
}
