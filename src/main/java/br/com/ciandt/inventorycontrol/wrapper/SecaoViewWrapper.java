package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Secao;
import lombok.Data;

@Data
public class SecaoViewWrapper {
    private Integer idSecao;
    private String tipoBebida;
    private Double quantidadeArmazenadaEmLitros;

    public SecaoViewWrapper(Secao secao) {
        this.idSecao = secao.getId();
        if (secao.getTipoBebidaArmazenada() != null){
            this.tipoBebida = secao.getTipoBebidaArmazenada().getDescricao();
        }else {
            this.tipoBebida = "ainda não armazenado";
        }
        if (secao.getCapacidaLitros() != null){
            this.quantidadeArmazenadaEmLitros =
                    calculaQuantidadeArmazenada(secao.getCapacidaLitros(), secao.getTipoBebidaArmazenada());
        }else {
            this.quantidadeArmazenadaEmLitros = 0.0;
        }
    }

    private Double calculaQuantidadeArmazenada(Double disponivel, TipoBebida tipoBebida){
        return tipoBebida.getCapacidadeSecao() - disponivel;
    }
}
