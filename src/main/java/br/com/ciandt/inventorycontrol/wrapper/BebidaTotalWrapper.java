package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BebidaTotalWrapper {
    private String tipoBebida;
    private Double totalArmazenadoEmLitro;

    public BebidaTotalWrapper(TipoBebida tipoBebida, Double totalSoma) {
        this.tipoBebida = tipoBebida.getDescricao();
        this.totalArmazenadoEmLitro = totalSoma;
    }
}
