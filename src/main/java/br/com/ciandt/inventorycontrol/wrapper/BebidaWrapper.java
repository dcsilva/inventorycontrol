package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BebidaWrapper {

    private Double medida;
    private TipoBebida tipoBebida;
    private String nomeResponsavel;

    public BebidaWrapper(Double medida, String tipoBebida, String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
        this.medida = medida;
        this.tipoBebida = TipoBebida.valueOf(tipoBebida);
    }
}
