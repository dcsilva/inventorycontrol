package br.com.ciandt.inventorycontrol.wrapper;

import br.com.ciandt.inventorycontrol.domain.Sort;
import br.com.ciandt.inventorycontrol.domain.TipoOrdenacaoHistorico;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class ConsultaHistoricoWrapper {
    private TipoOrdenacaoHistorico tipoOrdenacao;
    private Sort sort;
}
