package br.com.ciandt.inventorycontrol.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@PropertySource("classpath:/application.yml")
@Component
@Configuration
public class InitialProperties {

    public static Locale locale;

    @Autowired
    public InitialProperties(@Value("${spring.mvc.locale}") Locale locale) {
        InitialProperties.locale = locale;
    }

    public static void setLocale(Locale locale) {
        InitialProperties.locale = locale;
    }
}