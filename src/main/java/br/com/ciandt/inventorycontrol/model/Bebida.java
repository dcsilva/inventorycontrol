package br.com.ciandt.inventorycontrol.model;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.wrapper.BebidaArmazenamentoWrapper;
import br.com.ciandt.inventorycontrol.wrapper.BebidaWrapper;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bebida {

    @Id
    @GeneratedValue
    private Integer id;

    private Double medidaLitro;

    @ManyToOne
    @JsonManagedReference
    private Secao secao;

    @Enumerated(EnumType.ORDINAL)
    private TipoBebida tipoBebida;

    public Bebida(Bebida bebida) {
        this.id = bebida.id;
        this.medidaLitro = bebida.medidaLitro;
        this.secao = bebida.secao;
        this.tipoBebida = bebida.tipoBebida;
    }

    public Bebida(BebidaWrapper bebidaWrapper) {
        this.medidaLitro = bebidaWrapper.getMedida();
        this.tipoBebida = bebidaWrapper.getTipoBebida();
    }

    public Bebida(BebidaArmazenamentoWrapper bebidaArmazenamentoWrapper) {
        this.tipoBebida = bebidaArmazenamentoWrapper.getTipoBebida();
        this.medidaLitro = bebidaArmazenamentoWrapper.getVolume();
    }
}
