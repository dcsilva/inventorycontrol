package br.com.ciandt.inventorycontrol.model;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Secao {

    @Id
    @GeneratedValue
    private Integer id;

    private Double capacidaLitros;

    @OneToMany
    @JsonBackReference
    private List<Bebida> bebidas;

    @ManyToOne
    @JsonManagedReference
    private Estoque estoque;

    @Enumerated(EnumType.ORDINAL)
    private TipoBebida tipoBebidaArmazenada;

    public Secao(Estoque estoque) {
        this.estoque = estoque;
    }

    public Secao(TipoBebida tipoBebida) {
        this.tipoBebidaArmazenada = tipoBebida;
    }
}
