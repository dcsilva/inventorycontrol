package br.com.ciandt.inventorycontrol.model;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.domain.TipoHistorico;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
public class Historico {

    @GeneratedValue
    @Id
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
    private TipoHistorico tipoHistorico;

    @Enumerated(EnumType.ORDINAL)
    private TipoBebida tipoBebida;

    private Date dataHora;

    @ManyToOne
    private Secao secao;

    private Double volume;

    private String responsavel;

    public Historico() {
        dataHora = new Date();
    }

    public Historico(TipoBebida tipoBebida) {
        Secao secao = new Secao(tipoBebida);
        this.secao = secao;
    }

    public Historico(Secao secao) {
        this.secao = secao;
    }
}

