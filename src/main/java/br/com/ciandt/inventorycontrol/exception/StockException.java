package br.com.ciandt.inventorycontrol.exception;

public class StockException extends RuntimeException {
    public StockException(String message) {
        super(message);
    }
}
