package br.com.ciandt.inventorycontrol.exception;

public class StockNotAvailableException extends StockException {
    public StockNotAvailableException(String message) {
        super(message);
    }
}
