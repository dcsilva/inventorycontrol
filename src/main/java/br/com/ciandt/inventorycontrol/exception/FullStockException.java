package br.com.ciandt.inventorycontrol.exception;

public class FullStockException extends StockException{
    public FullStockException(String message) {
        super(message);
    }
}
