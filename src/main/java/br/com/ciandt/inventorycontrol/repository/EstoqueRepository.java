package br.com.ciandt.inventorycontrol.repository;

import br.com.ciandt.inventorycontrol.model.Estoque;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstoqueRepository extends JpaRepository<Estoque, Integer>{
}
