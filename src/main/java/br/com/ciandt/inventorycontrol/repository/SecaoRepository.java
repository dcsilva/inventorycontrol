package br.com.ciandt.inventorycontrol.repository;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Secao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SecaoRepository extends JpaRepository<Secao, Integer>{
    public List<Secao> findAllByTipoBebidaArmazenada(TipoBebida tipoBebida);
}
