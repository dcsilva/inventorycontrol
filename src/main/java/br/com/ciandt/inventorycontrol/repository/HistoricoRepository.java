package br.com.ciandt.inventorycontrol.repository;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import br.com.ciandt.inventorycontrol.model.Historico;
import br.com.ciandt.inventorycontrol.model.Secao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricoRepository extends JpaRepository<Historico, Integer>{

    List<Historico> findAllBySecaoAndTipoBebida(
            Secao secao,TipoBebida tipoBebida);
}
