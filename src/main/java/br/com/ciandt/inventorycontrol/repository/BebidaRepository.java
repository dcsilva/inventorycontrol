package br.com.ciandt.inventorycontrol.repository;

import br.com.ciandt.inventorycontrol.model.Bebida;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BebidaRepository extends JpaRepository<Bebida, Integer>{
}
