package br.com.ciandt.inventorycontrol.handler;

import br.com.ciandt.inventorycontrol.exception.FullStockException;
import br.com.ciandt.inventorycontrol.exception.StockException;
import br.com.ciandt.inventorycontrol.exception.StockNotAvailableException;
import br.com.ciandt.inventorycontrol.properties.MessagesProperties;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private Logger logger;

    @Autowired
    private MessagesProperties messagesProperties;

    @ExceptionHandler(StockException.class)
    public ResponseEntity<?> handler(StockException e, HttpServletRequest request){
        if (e instanceof FullStockException){
            logger.error("Full Stock", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }else if (e instanceof StockNotAvailableException){
            logger.error("Stock not available", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }else {
            logger.error("An unexpected error occurred on Stock.", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleDefaultException(Exception e,
                                                    HttpServletRequest request){
        logger.error("An unexpected error occurred on Stock.", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(messagesProperties.getMessage("error.internal_server_error"));
    }
}