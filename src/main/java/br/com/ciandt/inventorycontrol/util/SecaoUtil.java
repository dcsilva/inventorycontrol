package br.com.ciandt.inventorycontrol.util;

import br.com.ciandt.inventorycontrol.domain.TipoBebida;
import org.springframework.stereotype.Component;

@Component
public class SecaoUtil {

    public Double calculaQuantidadeArmazenada(Double disponivel, TipoBebida tipoBebida){
        return tipoBebida.getCapacidadeSecao() - disponivel;
    }
}
