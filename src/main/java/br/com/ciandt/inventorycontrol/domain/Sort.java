package br.com.ciandt.inventorycontrol.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Sort {
    ASC(1, "asc"),
    DESC(2, "desc");

    private Integer cod;
    private String value;
}