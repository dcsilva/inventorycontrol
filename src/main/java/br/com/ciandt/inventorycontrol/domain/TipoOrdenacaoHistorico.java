package br.com.ciandt.inventorycontrol.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoOrdenacaoHistorico {
    DATA(1, "dataHora"),
    SECAO(2, "secao");

    private Integer cod;
    private String value;
}
