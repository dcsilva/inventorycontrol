package br.com.ciandt.inventorycontrol.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum  TipoHistorico {
    SAIDA(1, "Histórico de sáida."),
    ENTRADA(2, "Histórico de entrada.");

    private Integer cod;
    private String descricao;
}
