package br.com.ciandt.inventorycontrol.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum TipoBebida {

    ALCOOLICA(1, "Bebida alcoólica.", 500.0),
    NAO_ALCOOLICA(2, "Bebida não alcoólica.", 400.0);

    private Integer cod;
    private String descricao;
    private Double capacidadeSecao;
}
