package br.com.ciandt.inventorycontrol;

import br.com.ciandt.inventorycontrol.model.Estoque;
import br.com.ciandt.inventorycontrol.model.Secao;
import br.com.ciandt.inventorycontrol.repository.EstoqueRepository;
import br.com.ciandt.inventorycontrol.repository.SecaoRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class InventorycontrolApplication {

	@Autowired
	private EstoqueRepository estoqueRepository;

	@Autowired
	private SecaoRepository secaoRepository;

	public static void main(String[] args) {
		SpringApplication.run(InventorycontrolApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {
			Estoque estoque = estoqueRepository.save(new Estoque());
			List<Secao> secoes = new ArrayList<>();
			secoes.add(new Secao(estoque));
			secoes.add(new Secao(estoque));
			secoes.add(new Secao(estoque));
			secoes.add(new Secao(estoque));
			secoes.add(new Secao(estoque));
			estoque.setSecoes(secoes);
			estoqueRepository.save(estoque);
		};
	}
}
