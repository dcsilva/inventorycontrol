# inventorycontrol

## Introdução
A API inventorycontrol foi construída com a finalidade de gerir um estoque e armazenamento e bebida. Tendo em vista funções de entrada e saída de bebidas.

## Detalhes técnico
A API foi criada seguindo o modelo de API's RESTFULL, utilizando arquitetura de microserviços com Spring Boot, Sprind Data, H2DataBase, Lombok, Swagger, Powermock e todas as outras dependências já contidas no Spring Boot. 

## O que preciso para executar ?

 -  Java SE Development Kit
 - Maven 
 - Git

## Como faço para executar ?

 1. Primeiro você deve baixar o projeto utilizando a ferramenta GIT.
 2. Após baixar o projeto entre na pasta do mesmo, abra o terminal(caso o Sistema Operacional seja Linux) ou Prompt de Comando(caso seja Windows) .
 3. Execute o comando abaixo:

    mvn clean install

 4. Entre na pasta **target**, e digite o comando abaixo :
 
    java -jar inventorycontrol-0.0.1-SNAPSHOT.jar

 5. Após iniciar a aplicação você poderá testar as funcionalidade pelo Swagger no endereço local  http://localhost:8080/swagger-ui.html , lá você encontra mais detalhes sobre os métodos e parametros utilizados. 
